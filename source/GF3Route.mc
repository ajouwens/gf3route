using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
//using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;
//using Toybox.ActivityMonitor as Act;

class GF3Route extends Ui.WatchFace {
    var is24Hour = false;
    var siblings = [[2,4,5], [1,3,4,5,6], [2,5,6], [1,2,5,7,8], [1,2,3,4,6,7,8,9], [2,3,5,8,9], [4,5,8], [4,5,6,7,9], [5,6,8]];
    var colorBg  = Gfx.COLOR_WHITE;
    var colorAc  = Gfx.COLOR_BLACK;
    var colorSt  = Gfx.COLOR_YELLOW;
    var priorStartPosition;
    var priorMinute;
    var coords = [new[2],new[2],new[2],new[2],new[2],new[2],new[2],new[2],new[2]];
    var positions = new[4];
    var fakeDigits = new [5];
    var counterAll = 0;
    var counterNr1 = 0;

    var cx;
    var cy;
    var roundWatch = true;

    function initialize() {
        is24Hour = Sys.getDeviceSettings().is24Hour;
        priorStartPosition = -1;
        priorMinute = -1;
    }

    function onLayout(dc) {
        cx = dc.getWidth() / 2;
        cy = dc.getHeight() / 2;
        roundWatch = (cx.toDouble() /cy.toDouble() == 1.0d);
        if(roundWatch) {
            coords = [[cx-53,cy-53], [cx,cy-76], [cx+53,cy-53], [cx-76,cy], [cx,cy], [cx+76,cy], [cx-53,cy+53], [cx,cy+76], [cx+53,cy+53]];
        } else {
            coords = [[cx-60,cy-50], [cx,cy-50], [cx+60,cy-50], [cx-60,cy], [cx,cy], [cx+60,cy], [cx-60,cy+50], [cx,cy+50], [cx+60,cy+50]];
        }
    }

    function onUpdate(dc) {
        dc.setColor(colorBg, colorBg, colorBg);
        dc.clear();
        var clockTime = System.getClockTime();
        var hour= clockTime.hour;
        var minute = clockTime.min;
        var hourUI = GF3Helper.getHourUI(hour, is24Hour);

        var time = new [4];
        time[0] = (hourUI[0] < 10) ? 0         : hourUI[0].toString().substring(0,1);
        time[1] = (hourUI[0] < 10) ? hourUI[0] : hourUI[0].toString().substring(1,2);
        time[2] = (minute    < 10) ? 0         : minute.toString().substring(0,1);
        time[3] = (minute    < 10) ? minute    : minute.toString().substring(1,2);

        // Only once per minute.
        if (minute != priorMinute) {
            positions = getThePositions(priorStartPosition);
//            Sys.print(", positions=" + positions);
            priorMinute = minute;

            var random = -1;
            while (random < 10000 ) {
                random = Math.rand();
            }
            fakeDigits[0] = random.toString().substring(0,1);
            fakeDigits[1] = random.toString().substring(1,2);
            fakeDigits[2] = random.toString().substring(2,3);
            fakeDigits[3] = random.toString().substring(3,4);
            fakeDigits[4] = random.toString().substring(4,5);
//            Sys.println("fakeDigits="+fakeDigits);
//            counterAll++;
//            Sys.println("counterAll="+counterAll + ", counterNr1=" + counterNr1);
        }

        // Draw the "routes"
        for(var i = 1; i < positions.size(); i++) {
            var x1 = coords[positions[i-1]-1][0];
            var y1 = coords[positions[i-1]-1][1];
            var x2 = coords[positions[i]-1][0];
            var y2 = coords[positions[i]-1][1];
            dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);

            if (x1 == x2) {
                dc.drawLine(x1-1, y1, x2-1, y2);
                dc.drawLine(x1  , y1, x2  , y2);
                dc.drawLine(x1+1, y1, x2+1, y2);
            } else if (y1 == y2) {
                dc.drawLine(x1, y1-1, x2, y2-1);
                dc.drawLine(x1, y1  , x2, y2  );
                dc.drawLine(x1, y1+1, x2, y2+1);
            } else if ((x1 < x2 && y1 < y2) || (x1 > x2 && y1 > y2)) {
                dc.drawLine(x1  , y1  , x2  , y2  );
                dc.drawLine(x1-1, y1+1, x2-1, y2+1);
                dc.drawLine(x1+1, y1-1, x2+1, y2-1);
                dc.drawLine(x1-1, y1  , x2  , y2+1);
                dc.drawLine(x1  , y1-1, x2+1, y2  );
            } else {
                dc.drawLine(x1  , y1  , x2  , y2  );
                dc.drawLine(x1-1, y1+1, x2-1, y2-1);
                dc.drawLine(x1+1, y1+1, x2+1, y2+1);
                dc.drawLine(x1+1, y1  , x2  , y2+1);
                dc.drawLine(x1  , y1+1, x2-1, y2  );
            }
        }

        var j = 0;
        for(var i = 0; i < coords.size(); i++) {
            var posX = coords[i][0];
            var posY = coords[i][1];
            var firstDigit = false;
            var isTimeDigit = false;
            var text = "0";

            if(i+1 == positions[0]) {
                text = time[0];
                firstDigit = true;
                isTimeDigit = true;
            } else if(i+1 == positions[1]) {
                text = time[1];
                isTimeDigit = true;
            } else if(i+1 == positions[2]) {
                text = time[2];
                isTimeDigit = true;
            } else if(i+1 == positions[3]) {
                text = time[3];
                isTimeDigit = true;
            } else {
                text = fakeDigits[j];
                j++;
            }

            var circle = 20;
            if(roundWatch) {
                circle = 24;
            }

            if(isTimeDigit) {
                dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
                dc.fillCircle(posX, posY, circle);
                var color = firstDigit ? colorSt : colorBg;
                dc.setColor(color, Gfx.COLOR_TRANSPARENT);
                dc.fillCircle(posX, posY, circle-5);
            }
            dc.setColor(colorAc, Gfx.COLOR_TRANSPARENT);
            dc.drawText(posX, posY, Gfx.FONT_LARGE, text.toString(), Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        }
    }

    function getThePositions(excludedDigit) {
        positions = new[4];
        positions[0] = getFirstPosition(excludedDigit);
        positions[1] = getNextPosition(positions, 0);
        positions[2] = getNextPosition(positions, 1);
        positions[3] = getNextPosition(positions, 2);
        return positions;
    }

    function getFirstPosition(excludedDigit) {
        var startPosition = GF3Helper.getRandomNumber(excludedDigit);
        priorStartPosition = startPosition;
        return startPosition;
    }

    function getNextPosition(positions, i) {
        var posSib = siblings[positions[i] - 1];
        return GF3Helper.getRandomNumberFromList(positions, posSib);
    }
}
