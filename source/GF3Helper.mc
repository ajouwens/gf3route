using Toybox.System as Sys;
using Toybox.Math as Math;

class RandomNumberCalculationError extends Toybox.Lang.Exception {
}

module GF3Helper {
    function getHourUI(hour, is24Hour) {
        if (!is24Hour) {
            if(hour == 0) {
                return [12, "AM"];
            } else if(hour == 12) {
                return [12, "PM"];
            } else if(hour < 12){
                return [hour.toNumber(), "AM"];
            } else {
                return [(hour.toNumber() % 12), "PM"];
            }
        }
        return [hour.toNumber(), ""];
    }

    function getRandomNumber(excludedNumber) {
        var digit = -1;
        while (digit < 1 || digit == excludedNumber) {
            digit = getMathRandomNumber();
        }
        return digit;
    }

    function getRandomNumberFromList(positions, siblings) {
//        Sys.println("");
//        Sys.print("positions=" + positions + ", siblings=" + siblings);
        if(siblings.size() == 1) {
            return siblings[0];
        }

        var i = -1;
        var index = getMathRandomNumber();
//        Sys.print(", index=" + index);
        while (index > siblings.size() - 1 || isDigitAlreadyAPosition(positions, siblings[index])) {
            i++;
            index = i;
            if(index > 3) {
                throw new RandomNumberCalculationError("positions=" + positions + ", siblings=" + siblings + ", index=" + index);
            }
        }
        return siblings[index];
    }

    hidden function getMathRandomNumber() {
        var random = Math.rand().toString();
        return random.substring(random.length()-1,random.length()).toNumber();
    }

    hidden function isDigitAlreadyAPosition(positions, digit) {
        for(var i = 0; i < positions.size(); i++) {
            if(positions[i] == digit) {
                return true;
            }
        }
        return false;
    }
}
