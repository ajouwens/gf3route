GF3Route - Digital watch
========================

Description
-----------
Digital watch face, with shows the current time as a route. 
Start with the colored circle and follow the route to read the time.
Each minute there's a new, completely random route.
--------------
Please, leave your comments, bug reports and ideas on this forum:
https://forums.garmin.com/showthread.php?265723-Watchfaces-GF3-
--------------
Version 1.0:
- First version
